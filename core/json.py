import ujson


def dumps(data, **kwargs):
    return ujson.dumps(data, ensure_ascii=False, indent=4, **kwargs)


def loads(data, **kwargs):
    return ujson.loads(data, **kwargs)


class Deserializable:
    public_fields = []

    def deserialize(self, *, include=None, exclude=None):
        include = include or []
        exclude = exclude or []

        result = {}

        for key in self.public_fields + include:
            if key not in result and key not in exclude:
                value = getattr(self, key)
                if value is None:
                    continue
                if hasattr(value, 'deserialize'):
                    value = value.deserialize()
                result[key] = value
        return result
