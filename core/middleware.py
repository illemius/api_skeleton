from aiohttp.web_exceptions import HTTPException, HTTPRedirection

from core import json


async def api_factory(app, handler):
    async def middleware_handler(request):
        try:
            return await handler(request)
        except HTTPRedirection:
            raise
        except HTTPException as e:
            e.text = json.dumps({'ok': False, 'code': e.status_code, 'message': str(e)})
            e.content_type = 'application/json'
            raise e

    return middleware_handler


async def cors_factory(app, handler):
    async def meddleware_handler(request):
        response = await handler(request)
        response.headers.update({
            'Access-Control-Allow-Origin': '*',
            'Access-Control-Allow-Credentials': '*'
        })
        return response
    return meddleware_handler
