class Environment:
    def __init__(self):
        self._data = {}

    def set(self, key, value):
        self._data[key] = value
        return self

    def get(self, key, default=None):
        return self._data.get(key, default)

    def delete(self, key, no_error=True):
        try:
            del self._data[key]
        except KeyError:
            if not no_error:
                raise

    def update(self, data=None, **kwargs):
        self._data.update(data, **kwargs)

    def __getitem__(self, item):
        return self.get(item)

    def __setitem__(self, key, value):
        self.set(key, value)

    def __delitem__(self, key):
        self.delete(key, False)
