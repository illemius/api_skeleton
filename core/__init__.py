import asyncio
import logging
import os
import sys

from aiohttp import web
from events import Events
from playhouse.postgres_ext import PostgresqlExtDatabase

import settings
from core.middleware import api_factory, cors_factory
from core.modules import ModulesLoader
from core.reload import TouchReloadWatcher
from core.utils.config import Config, find_config
from core.utils.enironment import Environment

__version__ = '0.1.0'
APP_NAME = 'GDL App Backend'

BASE_DIR = os.path.split(os.path.dirname(__file__))[0]

log = logging.getLogger(__name__)
events = Events(('on_load', 'on_start', 'on_stop'))
env = Environment()

PROC_TITLE = APP_NAME + ' [' + ' '.join(sys.argv) + ']'

modules = ModulesLoader()
db = PostgresqlExtDatabase(**settings.POSTGRES)

loop = asyncio.get_event_loop()
app = web.Application(middlewares=[
    api_factory,
    cors_factory
])

watcher = TouchReloadWatcher([os.path.join(BASE_DIR, 'me')],
                             timeout=settings.CHECK_TIMEOUT,
                             sleep=settings.BEFORE_RELOAD_SLEEP,
                             restart=settings.AUTO_START,
                             loop=loop)
watcher.configure_app(app)
