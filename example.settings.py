TIMEZONE = 'utc'

POSTGRES = {
    "database": "database",
    "user": "user",
    "password": "password"
}

# Touch-reload
AUTO_START = True
BEFORE_RELOAD_SLEEP = 2
CHECK_TIMEOUT = 5
