from aiohttp import web

import core
from .models import User
from .web import APIView


class HomeView(APIView):
    login_required = False

    async def get(self):
        return {'version': core.__version__}


class LoginView(APIView):
    login_required = False

    async def post(self):
        data = await self.request.post()

        if 'email' in data and 'password' and data:
            user = User.authenticate(data.get('email'), data.get('password'))
        elif 'token' in data:
            user = User.authenticate_token(data.get('token'))
        else:
            raise web.HTTPBadRequest(reason='Token or email with password is required!')

        return user.deserialize(include=['token'])


class RegisterView(APIView):
    login_required = False

    async def post(self):
        data = await self.request.post()

        if data.get('password1', '') != data.get('password2', ''):
            raise web.HTTPBadRequest(reason='Passwords don\'t match!')

        user = User.register(
            email=data.get('email', ''),
            password=data.get('password1', ''),
            first_name=data.get('first_name'),
            last_name=data.get('last_name'),
        )

        return user.deserialize(include=['token'])


class TestView(APIView):
    login_required = True

    async def get(self):
        return {'number': 42, 'bool': True, 'None': None,
                'user': (await self.get_user()).deserialize()}
