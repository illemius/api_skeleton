import core
from . import views


async def on_startup(app):
    app.router.add_route('*', '/', views.HomeView, name='home')
    app.router.add_route('*', '/user/login', views.LoginView, name='login')
    app.router.add_route('*', '/user/register', views.RegisterView, name='register')
    app.router.add_route('*', '/user/test', views.TestView, name='test')


core.app.on_startup.append(on_startup)
