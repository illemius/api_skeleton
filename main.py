#!/usr/bin/env python3
import argparse
import asyncio
import atexit
import logging
import signal
import sys
from setproctitle import setproctitle

import core
import settings
import conf
from core.database import create_tables
from core.prepare import set_timezone, setup_logging, start_web_server
from core.scripts import load_script
from core.version import print_versions

parser = argparse.ArgumentParser(description="aiohttp server example")
parser.add_argument('--runserver', '-r', action='store_true', default=False, help='Start web server')
parser.add_argument('--no-static', action='store_true', default=False, help='Disable serving static files')
parser.add_argument('--sock', help='UNIX Socket path')
parser.add_argument('--host', help='Webserver host')
parser.add_argument('--port', type=int, help='Webserver port')
parser.add_argument('--scripts', '-s', nargs='*', default=[], help='Load scripts', metavar='SCRIPTS')
parser.add_argument('--version', '-v', action='store_true', default=False, help='Version info')


def on_stop():
    core.events.on_stop()
    core.loop.stop()
    for task in asyncio.Task.all_tasks():
        task.cancel()


def execute():
    if len(sys.argv) == 1:
        parser.print_help()
        sys.exit(1)

    args = parser.parse_args()

    atexit.register(on_stop)
    core.loop.add_signal_handler(signal.SIGINT, on_stop)

    setproctitle(core.PROC_TITLE)
    set_timezone()
    setup_logging()

    core.env['no_static'] = args.no_static

    if args.version:
        print_versions()
        exit()

    for module_name in conf.INSTALLED_APPS:
        if not module_name or module_name and module_name.startswith('-'):
            continue
        core.modules.load_module(module_name)

    create_tables()
    core.events.on_load()

    if args.scripts:
        core.log.warning("Start scripts: " + ', '.join(args.scripts))
        for script in args.scripts:
            load_script(script)

    if args.runserver:
        core.events.on_start()
        start_web_server(path=args.sock, host=args.host, port=args.port,
                         print=logging.getLogger('aiohttp.access').warning)


if __name__ == '__main__':
    print(core.PROC_TITLE)
    execute()
